Meeting Sarah
 
Daniel Stoltz sat comfortably in the back of his Limousine as it conveyed him through town. He held a phone to his ear, its spiral cord leading to the center console next to him. It was the pinnacle of modern fast-track life: a phone in the car. Enviable technology, and Daniel had taken to it instantly despite the exorbitant costs.
“Hello? Daniel Stoltz here.”
“Mr. Stoltz,” came the voice from the other end. “This is Maria Towne of the San Francisco Chronicle. Do you have a moment to chat?”
“Sure, why not? I’m out on an errand right now but we can talk for a minute if you like.”
“Thank you for taking the time,” said Maria. “First question: What is the secret to your success? You have a large personal stake in companies such as Microsoft and Macintosh.”
“Actually, Macintosh is the name of a product,” he said. “The manufacturer’s name is ‘Apple’.”
“All right, I’ll note that down. Anyway, your investments have proven to be perfect. Everything you fund turns a huge profit. Some say you have the Midas touch. What do you say to that?”
“I don’t see how I could,” he said. “I don’t do anything. I just let them get on with their business and don’t interfere.”
“How do you choose what companies to invest in?”
He switched the phone to his other ear. “Oh I’m just like any other investor. I look at business plans and come to an informed decision.”
“Let’s turn the clock back a bit,” she said. “In college, you were a theoretical physics major working on your Ph.D. But you quit grad school and invested your then-modest savings into various high-risk ventures. Why the sudden change of lifestyle?”
 “Oh you know how it is. I was twenty-four years old and I decided to change my life. Happens all the time.”
“Several experts in the field say you had innovative ideas. Ideas that could have revealed genuine understanding about the nature of time itself. They even talk about time manipulation.”
Daniel sighed. “I did one equation about the curvature of space and people got all worked up about time travel. I guess it makes for good press, but it’s just theoretical masturbation.”
“But you wrote some detailed papers on it,” she pressed. “In one paper you state it could be possible to ‘rewind’ time, sending your current consciousness back to a younger version of yourself.”
“I was smoking a lot of marijuana back then,” he said. “I never understood why people take that paper seriously.”
“You patented your experimental equipment, then never allowed anyone to reproduce it under threat of lawsuit. It completely stymied any further research on that line. Why did you do that?”
“I’m a businessman,” he said. “The lensing apparatus I invented could have real value in the marketplace. It was a step on the road to extremely accurate timekeeping. Of course I patented it; it might be worth millions someday.”
She shuffled some papers on her end, then continued. “On a lighter note, let me ask this: If you could rewind time like that, would you?”
“Well,” he said. “There would be some serious downsides.”
“Like what?”
“You’d have to re-make all the relationships you had. At least, the ones that are important to you. Let me ask you, are you married?”
“Yes I am. Fourteen wonderful years.”
“When did you meet your husband?”
“Well, we dated for two years before getting married. So I guess it would be sixteen years ago.”
“All right,” Daniel said. “Imagine you rewound time twenty years. Now you have to wait four years just to meet him again. And even when you do, you have to do everything just right. If you mess up, he might not be interested, or he might end up with some other woman. See the problem?”
“I hadn’t thought of that,” she said.
“If you value your personal relationships, rewinding time would be risky.”
“Sorry, I got a little sidetracked,” she said. “Back to the real interview questions: I hear rumors that you’re helping fund a new company. SystemCo? Something like that?”
“It’s called Cisco. Yes, I have great confidence in them,” Daniel said.
The Limo pulled up to a dilapidated apartment complex and parked at the curb. The driver cocked his head toward Daniel and said “We’re here, sir.”
Daniel nodded and returned his attention to the phone. “I have to go. I’ve got an important meeting. Anything else?”
“No that’s great. Thanks for taking the time to do this interview, Mr. Stoltz. I know how busy you are.”
“No problem. Thanks, bye.” He hung up.
The driver exited the car and walked around to open Daniel’s door. “Shall I accompany you, sir? This neighborhood doesn’t look entirely safe.”
“Won’t be necessary, Charles,” said Daniel.
“Very well. I will endeavor to keep people from stealing the tires.”
“Good man.” Daniel said.
Unkempt, overgrown grass intruded on the footpaths as Daniel walked casually through the maze of structures. Paint peeled from every building, but the spray-paint graffiti was fresh enough. Bars had been installed on most of the windows.
He made his way to one specific unit and knocked on the door. After a few seconds, it opened.
The woman who answered it was beautiful. Raven hair fell across her perfect face as she pulled the door open a crack. She peered suspiciously through the opening at first, but once she saw Daniel’s finely-tailored clothes, she relaxed a little. Men in designer suits rarely robbed low-income apartments.
“Hi,” she said.
Daniel clasped his hands behind his back. “Hello. My name is Daniel Stoltz. We’ve never met, but I’m actually from this neighborhood. I used to live two buildings over.”
“You used to live here?” she said. “You don’t look like you’re from around here.”
“It was in another life,” he said. “Things are different for me now.”
“Yeah, I guess so,” she said. “What can I do for you?”
He smiled and said “I understand your cat just had a litter of kittens?”
Back at the Limo, Charles watched warily for any potential car thieves. He breathed a sigh of relief when he saw his employer returning from the complex. Then he raised an eyebrow when he saw Daniel was cradling a tiny kitten in his arms.
Charles opened the door and bowed curtly. “A new friend, Mr. Stoltz?”
“A very good friend,” Daniel said. “Take us back to the mansion please, Charles.”
“With pleasure, sir,” Charles said, closing the door.
Daniel made a platform with his hands and held the kitten up to his face. She rubbed a fuzzy cheek against his chin and purred loudly.
He smiled and stroked her fur. “Nice to see you again, Sarah. I’ve missed you.”