Robert A. Heinlein
Ti, mind, zombik



2217 -- IDŐZÓNA V (EST) -- 1970 Nov 7 -- NYC -- "Papa Bárja"

Épp egy brandys poharat törölgettem, amikor a Lányanya belépett a bárba. Följegyeztem az időt, 22:17, ötös zóna vagy keleti idő, 1970. november 7. Az időügynökök mindig feljegyzik a dátumot és időt, ez előírás.

A Lányanya 25 körüli férfi volt, nem magasabb nálam, éretlen vonások, érzékeny alkat. Nem tetszett, de ez nem is volt fontos. Miatta voltam itt, be kellett soroznom, az én srácom volt, ezért a legmegnyerőbb csapos mosolyomat öltöttem magamra.

Talán túl kritikus vagyok. Nem volt túl megnyerő, a becenevét a kíváncsiskodóknak adott válaszából kapta: "Lányanya vagyok." Ha nem volt gyilkos kedvében, hozzátette: "-- szavanként 4 centért önvallomásokat írok."

Ha cefetül érezte magát, csak arra várt, hogy beleköthessen valakibe. A stílusa halálos volt, mint egy rendőrnőé - ezért is kellett nekem. De nem ez volt az egyetlen ok.

Ledöntötte az italát és látszott, hogy most még a szokásosnál is jobban gyűlöli az embereket. Csendben kitöltöttem egy dupla whiskyt és otthagytam az üveget. Megitta, újratöltöttem.

Letöröltem a pultot. "- Mi ez a 'Lányanya' sztori?"

Az ujjai megfeszültek a poharán s úgy tűnt, nyomban hozzám vágja. A kezem az ólmosbotra simult a bárpult alatt. Az időmanipuláció során megpróbálsz mindent előre megtervezni, de olyan sok tényező van, hogy jobb nem kockáztatni.

Láttam, hogy kissé lenyugszik, a kiképzésen megtanítottak figyelni erre. "Bocs - mondtam - csak érdekel, hogy megy az üzlet, de beszélgethetünk az időjárásról is."

Mogorván nézett rám. "Elmegy. Írok, kiadják, van mit ennem."

Töltöttem, s elé toltam a poharat. "Igazából tetszenek az írásaid, olvastam párat. Úgy látom, jól megérted a nőket."

Ez olyan botlás volt, amit meg kellett kockáztatnom. Soha nem árulta el, milyen álnév alatt írja a történeteit, de eléggé maga alatt volt, hogy ez ne tűnjön fel neki. "Megértem a nőket?!" hördült fel, "Igen, ismerem nézőpontjukat. Nyilván."

"Miért?", kérdeztem habozva. - Húgok, nővérek?

- Nem. Nem hinnéd el, ha elmondanám.

- Hát, tudod - mondtam nyugodtan - egyet megtanultam a pult mögött: a valóság néha furább a meséknél. Fiam, ha hallanád a sztorikat, amiket nekem mesélnek, meggazdagodhatnál belőle. Hihetetlen.

- Tudod is te, mi az, hogy hihetetlen!

- És? Engem már semmi nem lep meg, biztos, hogy hallottam már furcsábbat.

Ismét felhorkant: "Akarsz fogadni a maradék italba?"

- Ebben fogadok - mondtam, s a pultra tettem egy bontatlan üveget.

- "Hát..." - jeleztem a másik pultosnak a fogadásunkat. A bár távolabbi végében volt egy széknyi hely, amit a pultra halmozott ecetes tojásos üvegekkel és más lomokkal tettem meghittebbé. Néhányan a bár másik végében a meccset nézték, valaki a zenegéppel babrált. Nem hallhattak minket. - "Hát jó, akkor kezdem az elején, törvénytelen gyerek vagyok."

- Úgy értem - tette hozzá -, hogy a szüleim nem voltak házasok.

- Nem nagy dolog, az enyémek sem.

- Amikor... - elhallgatott, s ekkor nézett rám először barátságosan. - Úgy érted?

- Úgy. Száz százalékos törvénytelen gyerek vagyok. Igazából, - tettem hozzá - a családomban senki sem házas. Mind fattyú.

- Ne akarj átrázni, nős vagy! - s gyűrűmre mutatott.

- Ó, ez? - mutattam neki - Hamis, azért hordom, hogy lerázzam a nőket. - Egy antik gyűrű volt, amit 1985-ben vettem egy munkatársamtól. Egy kereszténység előtti sírból szerezte. A világkígyó, ami a saját farkába harap, végtelen. A nagy paradoxon szimbóluma.

Alig vetett rá egy pillantást. - Ha tényleg törvénytelen gyerek vagy, - folytatta - tudod, milyen érzés. Amikor még kislány voltam...

- Micsoda? Jól hallottam?

- Nem érdekel a történetem? Amikor kislány voltam... Tudod, kicsoda Christine Jorgenson? Roberta Cowell?

- Uh, nemváltó esetek? Azt akarod mondani...

- Ne szakíts félbe, vagy nem folytatom. Talált gyerek voltam, egy árvaház lépcsőjén hagytak Clevelandben, 1945-ben, egy hónapos koromban. Kiskoromban irigy voltam azokra, akiknek voltak szülei. Aztán, amikor megismerkedtem a szexszel, s higgy nekem, Papa, egy árvaházban ez hamar megtörténik...

- Tudom.

- Megesküdtem, hogy ha gyereket szülök, lesz apja és anyja. Tiszta maradtam. S ez nem kis dolog abban a közegben - megtanultam bunyózni, hogy megvédjem magam és a fogadalmamat. Később rájöttem, hogy kevés esélyem lesz valaha is férjhez menni - ugyanazért, amiért nem fogadtak örökbe. Lóarcú, kapafogú, lapos mellű és egyenes hajú voltam.

 Nálam most is jobban mutatsz.

- Kit izgat egy csapos külseje? Vagy egy íróé? De az emberek a kis kék szemű, aranyszőke hajú ostobákra vágynak. És később a fiuk meg nagy cicikre és telt ajkakra, a tökéletes kiegészítőkre. - Megrántotta a vállát. - Esélyem sem volt. Ezért elhatároztam, hogy csatlakozok az Angyalokhoz.

- A mihez?

- Nemzeti női segélyszolgálat, vendéglátó és szórakoztató részleg, vagy ahogy ma hívják, az Űrangyalok. Kisegítő ápoló csoport, űrhajós légió.  

Ismertem mindkét megnevezést, egyszer jelentést írtam róla. Bár manapság egy harmadik néven nevezik őket: Elit katonai szolgáltató hadtest. A szótárváltozás a legnagyobb akadály az időutazásban - tudtad, hogy "töltőállomásnak" egykor a kőolajszármazékokat forgalmazó kutakat nevezték? Valamikor a Churchill-korszakban egy nő azt mondta nekem: Találkozzunk a töltőállomáson a szomszédban, - ami nem az, aminek hangzik. A töltőállomásokon (akkoriban) nem volt ágy.

- Ez akkoriban volt, folytatta, mikor a fejesek beismerték, hogy nem lehet évekre az űrbe küldeni egy férfit úgy, hogy ne tudja levezetni a feszültséget. Biztosan emlékszel, milyen hangos volt az ellenzék. Kevés volt az önkéntes, ez javította az esélyeimet. Jóhírű lányokat kerestek, lehetőleg szüzeket (az alapoktól akarták kezdeni velük), mentálisan átlag feletti, érzelmileg stabil nőkre volt szükségük. De a legtöbb jelentkező prosti volt, vagy idegbeteg, aki egy 10 napos űrutazástól megtört volna. Ezért nem kellett csinosnak lennem. Azt ígérték, ha felvesznek rendbe hozzák a fogaimat, hullámokat tesznek a hajamba, megtanítanak járni, táncolni, hogyan kell szórakoztatóan hallgatni egy férfit - és kiképeznek a fő feladatra. Még plasztikai műtétet is bevetnek, ha szükséges - a fiainknak mindenből csak a legjobbat.

- De a legjobb, hogy elintézik, hogy ne eshess teherbe a szolgálat alatt, a végére úgyis majdnem hogy biztos, hogy megházasodsz. Az Angyalokat űrhajósok veszik el, mert egy nyelvet beszélnek.

- Amikor 18 lettem, elszegődtem egy családhoz. Nekik csak egy olcsó szolga kellett, de nem zavart, mert csak 21 évesen sorozhattak be. Házimunkát végeztem és esti iskolába jártam - azt mondtam, hogy folytatom a középiskolai gép- és gyorsírást, de valójában illemtanfolyamra jártam, hogy javítsam a sorozási esélyeimet.

- Aztán megismertem ezt a dörzsölt fickót a százdolláros bankóival. - Összevonta a szemöldökét. - Kötegszám hordta magánál a 100 dollárosokat, életemben nem láttam annyi pénzt. Azt mondta, hogy vigyázni fog rám.

- Tetszett nekem. Az első férfi, aki érdek nélkül kedves volt hozzám. Felhagytam az esti iskolával, hogy több időt tölthessünk együtt. Életem legboldogabb időszaka volt.

- Aztán egy este a parkban lecsúszott a nadrágom.

Nem folytatta, ezért rákérdeztem: "És aztán?"

- Aztán semmi! Soha többé nem láttam. Hazakísért és azt mondta: szeret. Jóéjt csókot adott és soha nem jött vissza. Ha meg tudnám találni, megölném. - mondta eltökélten.

- Hát, mondtam, el tudom képzelni, hogy érzel. De megölni, csak azért, mert hallgatott a természet hívó szavára? Te talán küzdöttél ellene?

- Huhh, mi köze ennek ehhez?

- Elég sok. Talán megérdemel pár eltört csontot azért, mert rád mászott, de...

- Rosszabbat is megérdemel! Csak várd ki, amíg meghallod a többit. Valahogy mégis képes voltam meggyőzni magam, hogy így lesz a legjobb. Nem is szerettem, csak viszonyunk volt, alig vártam, hogy csatlakozhassak a programhoz. Nem szórtak ki, nem érdeklődtek a szüzességem iránt. Örülni kezdtem.

- De egyszerre szűk lett a szoknyám, s tudtam, hogy mindennek vége van.

- Terhesség?

- A szemét teherbe ejtett! A sóher család nem foglalkozott vele, amíg el tudtam végezni a munkám - aztán kirúgtak. Az árvaházba nem mehettem vissza. Egy jótékonysági alapítvány anyaotthonába kerültem, egy nagy rakás duzzadó pocak és ágytál közé, míg rám nem került a sor.

- Egyik este egy műtőasztalon találtam magam és a nővér azt mondta: Nyugalom, lélegezzen mélyeket.

- Az ágyamban ébredtem, a mellkasomtól lefelé teljesen zsibbadtan. Jött a sebész, s vidáman kérdezte, hogy érzem magam. 

- Fáradtan.

- Ez természetes, kikészült és tele van nyugtatókkal. Egy császármetszés sosem egyszerű, de minden rendben van.

- Császármetszés? - kérdeztem - A kicsi jól van, doktor úr?

- Remekül.

- Ó, fiú lett vagy kislány?

- Egészséges kislány. Három kiló huszonöt deka.

Megnyugodtam. Azért az nem semmi, ha gyereked születik. Arra gondoltam, elmegyek messze, s azt mondom, meghalt az apja. A lányom nem megy árvaházba! 

De a szülész tovább folytatta: Mondja csak, ööö - kerülte a nevem - nem említette senki, hogy a belső szervei eltérnek az átlagostól?

- Micsoda? - kérdeztem - Természetesen nem. Ezt meg hogy érti?

Habozott. - Na jó, elmondom most, aztán az altató majd segít, hogy kipihenje magát. Szüksége lesz rá.

- Miért? - kérdeztem.

- Hallott valaha arról a skót orvosról, aki 35 éves koráig nőként élt? Majd néhány műtét után törvényileg és orvosilag férfi lett? Meg is nősült és boldogan él.

- Mi köze ennek hozzám?

- Épp az, amit mondok. Maga férfi.

Megpróbáltam felülni. - Micsoda?

- Csak nyugalom. A beavatkozás során valami nagyon érdekeset találtunk. Mondhatom, hogy egyedi dolgot. Miután sikeresen megszületett a baba, felhívtam a sebész főorvosunkat, konzultáltam vele, majd órákig dolgoztunk, hogy minél jobban helyreállítsuk. Két teljesen hiánytalan szaporítószerv-rendszere volt, fejletlenek, de a női eléggé fejlett ahhoz, hogy gyermeket szülhessen. A szülés alatt fellépő vérzés miatt el kellett távolítanunk a petefészkét és a méhét, de a műtét során létre tudtunk hozni egy férfi húgyvezetéket. További műtétekre is szükség lesz majd - a vállamra tette a kezét - Ne aggódjon, még fiatal, a csontozata majd alkalmazkodik, figyelni fogjuk a hormonszintjét. Ez nem halálos ítélet, teljes életet élhet.

Sírni kezdtem. - Mi lesz a babával?

- Hát, nem tudja ápolni, még egy kiscicának se lenne elég a teje. Ha a helyében lennék, én nem is akarnám látni, hanem örökbe adnám.

- Soha!

Vállat vont. - A döntés a magáé, ön az anyja - azaz a szülője. De most ne törődjön ezzel, előbb talpra kell állítsuk.

Másnap megengedték, hogy meglátogassam, s ezt minden nap megtettem. Próbáltam megszokni. Soha nem láttam még újszülöttet, fogalmam sem volt, mennyire visszataszítóak. A lányom úgy nézett ki, mint egy rózsaszín majom. Az érzéseim elhatározássá váltak, eldöntöttem, hogy a legjobbat fogja kapni mindenből. De egy hónap múlva ez már semmit sem jelentett.

- Hogyhogy?

- Elrabolták.

- Elrabolták?

A Lányanya majdnem feldöntötte a palackot, amiben fogadtunk. - Gyermekrablás. Elrabolták a rohadt kórházi szülészetről. - Nagyot sóhajtott. - Elvették az egyetlen dolgot, amiért még élni akartam.  

- Kemény. - értettem egyet - Hadd töltsek még egyet. Semmi nyom?

- Semmi, amit a rendőrség követhetett volna. Valaki meglátogatta, azt állította, hogy a nagybátyja. Amikor a nővér nem figyelt, egyszerűen kisétált a babával.

- Személyleírás?

- Egy férfi, átlagos vonásokkal, mint a tied vagy az enyém. - Felvonta a szemöldökét - Szerintem az apja volt. A nővér megesküdött, hogy idősebb férfit látott, de azt hiszem, csak sminket használt. Ki más lehetett volna? A gyermektelen nők gyakran tesznek ilyet, de ki hallott már olyanról, hogy egy férfi gyereket raboljon?

- Mit csináltál?

- Tizenegy hónapot töltöttem azon az átkozott helyen, átestem három komoly műtéten. Négy hónap után elkezdett nőni a szakállam, már rendszeresen borotválkoztam, mikor kiengedtek... s már nem kételkedtem abban, hogy férfi vagyok. - elvigyorodott - A nővérek egész jóképűnek tartottak.

- Hát - mondtam - én úgy látom, összekaptad magad. Egy normális férfi vagy, pénzt keresel, gondtalanul élsz. S a nők élete egyáltalán nem könnyű.

Rám bámult - Mintha te olyan sokat tudnál róla!

- Mert?

- Hallottad valaha az 'összetört nő' kifejezést?

- Hmmm, évekkel ezelőtt. Ma már nem használják.

- Az a szemét annyira összetört, amennyire egy nőt csak össze lehet törni... Nem voltam többé nő... és nem tudtam, hogyan legyek férfi.

- Megszokja az ember, azt hiszem.

- El sem tudod képzelni. Nem arra gondolok, hogyan kell öltözni, vagy hogy ne a rossz mosdóba menjek, ezt megtanultam még a kórházban. De hogyan éljek így? Milyen munkát vállalhatok? A pokolba, még csak jogosítványom sincs. Nem értek a kereskedelemhez, fizikai munkát nem tudok végezni, túl sok a sérült szövet, túl érzékeny vagyok.

- Gyűlöltem azért, hogy megfosztott a Angyalok nyújtotta lehetőségtől, bár arra, hogy mennyire, csak akkor döbbentem rá, mikor megpróbáltam újra csatlakozni az űrhadtesthez. Elég volt ránézni a hasamra, máris alkalmatlannak minősítettek katonai szolgálatra. Az egészségügyi tiszt azért kíváncsiságból megvizsgált, olvasott az esetemről.

- Mit tettél?

- Szóval nevet változtattam és New Yorkba jöttem. Először egy konyhán dolgoztam, aztán béreltem egy írógépet és gyorsíróként dolgoztam. Nevetséges! Négy hónap alatt négy levelet gépeltem le és egy kéziratot. A kézirat az 'Igaz történetek' magazinnak csak papírpocséklás volt, de a fickó el tudta adni. Ez adott egy ötletet: vettem egy csomó női magazint és alaposan áttanulmányoztam őket. - cinikusan nézett rám - Most már tudod, honnan van az az igazi lányanyai látásmódom... az egyetlen olyan sztori miatt, amit nem adtam el: az igazság miatt. Na, nyertem?

Felé toltam a palackot. Én magam is ideges voltam, de a munkámat el kellett végezzem. Megkérdeztem: - Fiam, ha találkoznál vele, még mindig megtennéd?

A szemeiben vadul csillogó fény gyulladt.

- Fogd vissza magad! - mondtam - Ugye, nem ölnéd meg?

Ördögien nevetett - Dehogynem.

- Nyugi. Többet tudok róla, mint gondolnád. Segíthetek neked. Tudom, hol van.

Megragadta az ingem. - Hol van?

Nyugodtan válaszoltam - Engedd el az ingem, fiacskám, vagy a sikátorban fogod találni magad, s azt mondjuk a rendőröknek, hogy elájultál. Megmutattam neki az ólmosbotot.

Elengedett. - Bocsánat. De hol van? - Rám bámult. - ...és honnan tudsz ennyit?

- Mindent a maga idejében. Vannak feljegyzések - kórházi, árvaházi és orvosi feljegyzések. Az árvaház vezetője Mrs. Fetherage volt, nem igaz? Őt Mrs. Gruenstein követte, igaz? A leánykori neved Jane volt, nemde? S minderről nem beszéltél nekem, ugye? 

Hagytam, hogy összezavarodjon és megijedjen. - Mi akar ez lenni? Bajba akarsz keverni?

- Dehogyis. A te oldaladon állok. A kezedbe tudom adni a fickót. Azt teszel vele, amit akarsz - s garantálom, hogy soha nem derül ki. De nem hiszem, hogy meg tudnád ölni. Nem vagy eléggé tökös hozzá.

A pultra csapott. - Pofa be. Hol van?

Egy rövidet töltöttem neki, már részeg volt, de a dühe erősebb. - Ne olyan gyorsan. Segítek neked - aztán te segítesz nekem.

- Hmmm... Miben?

- Nem szereted a munkádat. Mit szólnál magas fizetéshez, biztos álláshoz, végtelen költségkerethez, te lennél a saját főnököd és sok kalandos küldetés várna rád?

Rám bámult - Azt mondanám, hogy tüntesd el azt az istenverte rénszarvast a tetőmről. Cseszd meg, Papa, nincs is ilyen munka.

- Jó, akkor mondjuk így: a kezedbe adom a faszit, elrendezed vele a dolgod, aztán kipróbálod a munkámat. Ha nem olyan, mint mondtam - hát - nem foglak visszatartani.

Már bizonytalan volt az utolsó ital miatt. - Mikor tudod a kezembe adni? - kérdezte nehezen?

- Ha üzletet kötünk, azonnal.

Kezet fogtunk - Áll az alku!

Jeleztem a társamnak, hogy figyelje a bár mindkét végét, majd feljegyeztem az időpontot, 23:00 - s amikor átbújtam a bárpult alatt a zenegép az "Én vagyok a nagyapám" című dalt kezdte játszani. Szóltam a szerelőnek, hogy csak régi amerikai és klasszikus zenéket tegyen be, mert nem bírom a '70-es évek zenéjét, de nem tudtam, hogy ezt is betette. Odakiáltottam - Kapcsold ki és add vissza a vendég pénzét! - majd hozzátettem - Raktár, azonnal jövök! - megindultam, nyomomban a Leányanyával.

Lementünk a lépcsőn, innen nyílt egy vasajtó, amihez csak nekem és a főnökömnek volt kulcsa, mögötte pedig egy másik szoba, amihez csak nekem volt kulcsom. Oda mentünk.

Aggodalmasan kémelelte az ablaktalan falakat. - Hol van?

- Épp itt. - Kinyitottam a táskát, az egyetlen tárgyat a szobában, egy U.S.F.F. koordináta-transzformáló készüléket, 1992-es sorozat, II. verzió - nagyszerű készülék, nincs mozgó alkatrésze, 23 kiló teljesen föltöltve, s pont úgy néz ki, mint egy aktatáska. Már korábban beállítottam, csak a transzformáló mező határát képző fémháló hiányzott.

Ezzel foglalkoztam, mikor megkérdezte: - Mi a fene az?

- Időgép - feleltem és magunk köré húztam a hálót. - Hé! - kiáltotta hátrahőkölve. Van erre egy módszer: úgy kell a hálót a célpontra dobni, hogy mikor ösztönösen hátralép, körétek zárhasd a hálót, különben hátramarad a cipője, vagy a lába, esetleg magaddal viszed a padló egy részét. De ez csak gyakorlás kérdése. Pár ügynök be tudja csalni a célszemélyt a hálóba, én az őszinteség kiváltotta pillanatnyi megdöbbenést használom ki a kapcsoló elfordítására. Mint most is.


1030 -- V -- 1963 április 3. -- Cleveland, Ohio -- Apex épület.

- Hé, vedd le rólam ezt a vackot!

- Bocs, - mondtam, s begyömöszöltem a hálót a táskába - de azt mondtad, meg akarod találni.

- De hát... Azt mondtad, hogy ez egy időgép!

Az ablakra mutattam. - Miért, szerinted november van? És ez New York? - Míg tátogva a friss rügyeket és a kinti nyárias időt bámulta, kinyitottam a táskát, s kivettem egy köteg száz dollárost, ellenőriztem, hogy a dátumok és az aláírások megfelelnek-e 1963-nak. Az Időügynökséget nem érdekli, mennyit költesz (nem kerül semmibe), de nem szeretik a szükségtelen kortévesztést. Ha túl sokat hibázol, a hadbíróság száműz egy kellemetlen időszakba, mondjuk 1974-be, a szigorú fejadagokkal és a kényszermunkával. Soha nem követek el ilyen hibákat, a pénz rendben volt. Megfordult, s azt kérdezte: - Mi történt?

- Itt van. Menj és kapd el. Itt a költőpénz. - Kezébe nyomtam, és hozzátettem: - Intézd el. Majd jelentkezem.

A százdollárosok hipnotikus hatást gyakorolnak mindenkire, aki nincs hozzászokva. Döbbenten tapogatta miközben átvezettem a hallon s kitettem a szűrét. A következő csak egy könnyed kis ugrás volt.


17:00 --V -- 1964 március 10 -- Cleveland -- Apex épület.

Egy üzenet várt az ajtó alatt, hogy jövő héten lejár az albérletem, egyébként a szoba épp úgy nézett ki, mint egy pillanattal korábban. Odakint hó borította a fákat, siettem, csak a korabeli pénzért és a kabátomért álltam meg. A kalapot és a kabátot akkor hagytam itt, mikor kivettem a szobát. Béreltem egy kocsit és a kórházhoz hajtottam. Húsz percig untattam az ápolónőt, míg alkalmas pillanat adódott, hogy észrevétlenül kisétálhassak a csecsemővel. Visszamentünk az Apex épületbe. A tárcsa beállítása most nagyobb figyelmet igényelt, lévén az épület még nem állt 1945-ben, de számoltam ezzel az aprósággal.


0100 -- V -- 1945 szeptember 20 -- Cleveland -- Skyview Motel 

A mezőtranszformáló, a baba és én egy külvárosi motelbe érkeztünk. Korábban már bejelentkeztem náluk, mint "Gregory Johnson, Warren, Ohioból", így egy elsötétített, zárt ablakú és ajtajú szobába érkeztünk, a padló már előre meg volt tisztítva az érkezésünkre. Egy szék is csúnya sérülést tud okozni, ha nem ott van, ahol lennie kellene. Persze, nem a szék, hanem a mezővisszacsatolás miatt.

Minden rendben ment. Jane mélyen aludt, kivittem és egy bevásárlókosárba fektettem a kocsi ülésén, amit már korábban intéztem, majd az árvaházhoz hajtottam. A lépcsőre tettem, majd két tömbbel odébb, egy töltőállomásról (az üzemanyagos fajta) felhívtam az árvaházat. Visszamentem az időben, hogy lássam, amint beviszik az árvaházba, aztán a kocsit a motelnél hagyva 1963-ba ugrottam.


2200 -- V -- 1963 április 24 -- Cleveland -- Apex épület.

Egész jól választottam meg az időpontot - az időbeli pontosság a távolsággal arányos, egyetlen kivétel a zéró pontra visszatérés. Ha jól csináltam, Jane már rájöhetett, hogy nem is volt annyira 'aranyos' lány azon a nyárias éjszakán a parkban, mint gondolta. Fogtam egy taxit és a sóherek lakásához hajtattam. A sofőr a sarkon várt rám, amíg az árnyékokat kémleltem.

Az utcán találtam őket, kart karba öltve sétáltak. Felkísérte a tornácra és hosszú jó-éjt csókot adott neki - hosszabbat, mint amire számítottam. Miután elbúcsúztak és elindult, mellé léptem és karon fogtam. - Ez minden, fiam. - mondtam csendesen - Jöttem, hogy felvegyelek.

- Te! - a hangja elcsuklott a meglepetéstől.

- Én... Most már tudod, ki ő, s ha végiggondolod, tudni fogod, ki vagy... s ha elég erősen gondolkodsz, azt is ki fogod találni, ki a baba... s hogy ki vagyok én.

Nem válaszolt, de egész testében remegett. Ez a sokk is bizonyítja, hogy nem tudsz nem engedni a magad elcsábításának. Visszavittem az Apex épületbe, s ismét ugrottunk.


2300 -- VII -- 1985. augusztus 12. -- Földalatti bázis.

Felébresztettem az ügyeletes őrmestert, megmutattam az igazolványomat, megparancsoltam, hogy dugja ágyba egy altatóval, s reggel sorozza be. Az őrmester savanyúan nézett rám, de a rang mégis rang, bármelyik kort írjuk is, így azt tette, amit mondtam - nem kétlem, hogy következő alkalommal, ha találkozunk, ő lesz az ezredes és én az őrmester. Ez könnyen megeshet az alakulatunkban. - Neve? - kérdezte.

Lediktáltam, a hallatán felszaladt a szemöldöke. - Vagy úgy... Hmm...

- Tegye a dolgát őrmester. - A kíséretemhez fordultam, - Fiam, a gondjaidnak ezennel vége. Most kezded el a legjobb munkát, amit ember csak kaphat... és jó leszel benne. Biztosan tudom.

- De...

- Semmi de! Menj aludni, reggel majd megkapod a feladatod. Tetszeni fog.

- Így lesz! - értett egyet az őrmester. - Nézzen rám, 1917-ben születtem, még mindig itt vagyok, még mindig fiatal, élvezem az életet. - Visszatértem az ugró szobába és az összes tárcsát nullára állítottam.


2301 -- V -- 1970 november 7. -- Papa Bárja

Az ötödik Drambuie-vel jöttem ki a raktárból, hogy magyarázatul szolgáljon a távollétemre. A helyettesem azzal a vendéggel vitázott, aki az "Én vagyok a nagyapám"-at kérte. Azt mondtam: - Hagyd, hadd hallgassa végig, aztán húzd ki. - Nagyon elfáradtam.

Kemény, de valakinek ezt is meg kell csinálnia, s nem könnyű bárkit is beszervezni az 1972-es hiba után. Tudsz jobbat, mint olyan embereket keresni, akik undorodnak a koruktól, s adni nekik egy jól fizető munkát, ami, bár veszélyes, de érdekes és szükséges? Mindenki tudja már, miért csak sistergett 1963-ban a Sistergős háború. A New York-nak szánt bomba nem robbant fel, száz másik dolog nem úgy sikerült, ahogyan tervezték, s mindet hozzám hasonlóak intézték el.

De a '72-es kivétel, az nem a mi hibánk és nem lehet kijavítani, nincs paradoxon, amit megoldhatnánk. Valami vagy megtörténik vagy nem, most és mindörökké ámen. De többé nem lesz ilyen, az 1992-es utasítás biztosítja ezt.

Öt perccel korábban zártam, hagytam egy levelet a pénztárban a főnökömnek, hogy elfogadom az ajánlatát, ezért beszéljen az ügyvédemmel, mert én hosszú szabadságot veszek ki. Az irodai bürokrácia kissé lassú, de szeretnek mindent rendben tudni. Visszamentem a raktárba és 1993-ba ugrottam.


2200 -- VII -- 1993 január 12 -- Föld alatti központ:

Bejelentkeztem az ügyeletes tisztnél, és a körletembe mentem, hogy egy hétig aludjak. Magammal hoztam az üveget, amiben fogadtunk (végtére is megnyertem) és ittam egyet, mielőtt nekiláttam a jelentésemnek. Borzalmas íze volt, s elgondolkodtam, miért is szeretem a whiskyt, de jobb volt, mint a semmi. Nem szeretek teljesen józan lenni, mert túl sokat gondolkodom. De nem is húztam meg nagyon az üveget, az embereknek félelmeik vannak - nekem embereim.

Lediktáltam a jelentésem, negyven újoncot vesz fel a pszichológiai osztály - az enyémet is beleértve. Tudtam, hogy rábólintanak, hisz itt vagyok, nemde? Rögzítettem a kérelmem, hogy osszanak be operatív munkára, már beleuntam a beszervezésbe. Mindkettőt a nyílásba dobtam és lefeküdtem aludni.

A szemem "Az idő törvényei"-re tévedt az ágyam fölött.

Ne kapkodd el azt, ami holnap is ráér.
Ha végre sikerül, ne próbáld újra.
Egy öltés az időben 9 milliárdot menthet meg.
A különleges helyzetek különlegesen kezelhetők.
Korábban van, mint gondolnád.
A felmenők is csak emberek.
Még Jupiter is egyetért.

Már nem inspirálnak annyira, mint fiatal koromban, 30 évnyi időugrálás kikészít. Levetkőztem, s lepillantottam a hasamra. A császározás nagy hegeket hagy maga után, de már annyira szőrös voltam, hogy észre sem vettem, ha nem kerestem.

Aztán a gyűrűmre pillantottam.

A saját farkát felfaló kígyó, az örök körforgás... Azt tudom, hogy én honnan származom - de ti, zombik, honnan jöttök?

Éreztem, hogy rám tör a fejfájás, de soha nem veszek be fájdalomcsillapítót. Egyszer kipróbáltam és mind eltűntetek.

Inkább ágyba bújtam és lekapcsoltam a lámpát.

De hát nem is vagytok itt. Nincs itt senki más, csak én - Jane - egyedül a sötétben.

- Borzasztóan hiányzol.
